﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_App
{
    class AvailableLanguages
    {
        public enum Languages
        {
            English =1,
            Hausa,
            Igbo,
            Yoruba,
            French,
            Chinese
        }

        public string GetLanguage()
        {
            string selectedLanguage = null;
            string choice = null;
            Console.WriteLine("Welcome to Xpress Bank!\n Please select your prefered language:");
            LanguageList();
            choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    selectedLanguage = "English language";
                    break;
                case "2":
                    selectedLanguage = "Hausa language";
                    break;
                case "3":
                    selectedLanguage = "Igbo language";
                    break;
                case "4":
                    selectedLanguage = "Yoruba language";
                    break;
                case "5":
                    selectedLanguage = "French language";
                    break;
                case "6":
                    selectedLanguage = "Chinese language";
                    break;
                default:
                    selectedLanguage = "Default language";
                    break;
            }
            return selectedLanguage;
        }

        private void LanguageList()
        {
            string[] languages = Enum.GetNames(typeof(Languages));
           
            for (int i = 0; i < languages.Length; i++)
            {
                int id = i+1;
                Console.WriteLine($"{id}. {languages[i]}");
            }

        }
    }
}
