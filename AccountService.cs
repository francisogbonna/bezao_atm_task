﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_App
{
    public class AccountServices
    {
        //double amount;
        Authentication auth = new Authentication();
        public bool loggedIn = false;
        const double leastDemonination = 500;
        
        public AccountServices()
        {
            auth.Register();
        }
        public void CheckBalance()
        {
            double bal = auth.GetBalance();
            Console.WriteLine($"Your Account Balance is {bal}");
        }

        public void Login()
        {
            string loginIfo = auth.Login();
            loggedIn = !loginIfo.Contains("Invalid") ? loggedIn = !loggedIn : loggedIn = false;
            Console.WriteLine(loginIfo);
        }
        public string Withdraw()
        {
            
            double amount = 0;
            string message = "";
            double availableBal = auth.openingBal;
            Console.WriteLine("Enter the amount you want to withdraw:");
            amount = double.Parse(Console.ReadLine());
            double original_amount = amount;
            if(availableBal > amount)
            {
                do
                {
                    double available_note = amount - leastDemonination;
                    amount = available_note;
                } while (amount >= leastDemonination);
                if(amount != 0)
                {
                    Console.WriteLine($"Sorry you cannot withdraw {original_amount}. Available denominations are \nNGN 1,000 \nNGN 500");
                    message = "Please exit and try again.";
                    return message;
                }
                else
                {
                    message = "Please take your cash. \nThank you for Banking with Us!";
                    return message;
                }
            }
            else
            {
                message = $"Insufficient Fund.\nPlease exit and try again.";
                return message;
            }
        }

        public string Transfer()
        {
            string message = null;
            Console.WriteLine("Enter Amount:");
            double amount = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter Recipient's Account Number:");
            int reciever_account = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Account holder's Name:");
            string reciever = Console.ReadLine();

            if(amount > auth.openingBal)
            {
                message = "Insufficent fund. Transaction aborted";
            }
            else
            {
                message = $"Transfer successful.\n{amount} transfered to {reciever}";
            }
            return message;
        }
    }
}
