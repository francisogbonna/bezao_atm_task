﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_App
{
    public class AccountRegistration
    {
        public string userName;
        public string password;
        public double openingBal;


        public void Register()
        {
            userName = "Pope Francis";
            password = "user123";
            openingBal = 54000;
        }

        public string CreateAccount()
        {
            StringBuilder message = null;
            string[] users = new string[3];
            Console.WriteLine("Enter your fullname:");
            userName = Console.ReadLine();

            Console.WriteLine("Enter Opening Amount:");
            double.TryParse(Console.ReadLine(), out openingBal);

            Console.WriteLine("Enter your password:");
            string password = Console.ReadLine();
            Console.WriteLine("Confirm Password:");
            string confirm_password = Console.ReadLine();
            if (password != confirm_password)
            {
                Console.WriteLine("Password do not match.");
                string user = "Unable to create account. Please try again";
                return user;
            }
            else
            {
                users[0] = userName;
                users[1] = password;
                users[2] = openingBal.ToString();
                Console.WriteLine("Your account has been created successfully. Thanks for choosing Xpress Bank!");

                foreach (var user in users)
                {
                    message.Append($"{user} ");
                }
                
            }
            return message.ToString();
        }
        //public AccountRegistration(string user, string pass, double bal)
        //{
        //    userName = user;
        //    password = pass;
        //    openingBal = bal;
        //}

       
    }
}
