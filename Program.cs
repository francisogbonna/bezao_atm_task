﻿using System;
using System.Threading;

namespace ATM_App
{
    class Program
    {
        static void Main(string[] args)
        {
            AccountServices bankServices = new AccountServices();
            AvailableLanguages language = new AvailableLanguages();

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("\n\t\tXpress Banking services ...your money is in safe hands!\n");
            Console.ResetColor();
            string chose_language =language.GetLanguage();
            Console.WriteLine($"You've selected {chose_language}");
            Thread.Sleep(5000);
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("\n\t\tXpress Banking services ...your money is in safe hands!\n");
            Console.ResetColor();

            Console.WriteLine("Welcome Please login to perform transaction");
            bankServices.Login();
            
            if(bankServices.loggedIn == true)
            {
                string response = null;
                string choice = null;
                Console.WriteLine("Please select your prefered operations:\n1.Check Balance\n2.Withdraw fund\n3.Transfer fund\n4.Utility payment");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        bankServices.CheckBalance();
                        response = "Thanks for Banking with Us";
                        break;
                    case "2":
                        response = bankServices.Withdraw();
                        break;
                    case "3":
                        response = bankServices.Transfer();
                        break;
                    case "4":
                        response = "Sorry this service is not available at the moment.";
                        break;
                    default:
                        response = "Invalid Service selection. Please exit and try again.";
                        break;
                }
                Console.WriteLine(response);
            }
            else
            {
                Console.WriteLine("Please exit and try login again.");
            }



            Console.ReadLine();
        }
    }
}
